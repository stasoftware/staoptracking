using System.ComponentModel.DataAnnotations;

namespace StaOpTracking.Data.Models;

public class Employee : Entity
{

    [Required] 
    public string Firstname { get; set; }
    public string? Lastname { get; set; }
    public string? Gender { get; set; }
    [Required] 
    public string Email { get; set; }
    public DateTime Birthdate { get; set; }

    public string? Lat { get; set; }
    public string? Lon { get; set; }
    
    public Company? Company { get; set; }
}