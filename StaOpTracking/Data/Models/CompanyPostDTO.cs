using System.ComponentModel.DataAnnotations;

namespace StaOpTracking.Data.Models;

public class CompanyPostDTO
{
    public long Id { get;  set; }
    public string Name { get; set; }
}