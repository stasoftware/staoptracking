using System.ComponentModel.DataAnnotations;

namespace StaOpTracking.Data.Models;

public class CompanyDTO
{
    public long Id { get;  set; }
    public string Name { get; set; }
    public virtual ICollection<EmployeeDTO> Employees { get; set; }
}