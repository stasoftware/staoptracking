
namespace StaOpTracking.Data.Models;

public class EmployeeDTO
{
    public long Id { get;  set; }
    public string Firstname { get; set; }
    public string? Lastname { get; set; }
    public string? Lat { get; set; }
    public string? Lon { get; set; }
    public CompanyDTO? Company { get; set; }
}