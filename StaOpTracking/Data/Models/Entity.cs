using System.ComponentModel.DataAnnotations;

namespace StaOpTracking.Data.Models;

public abstract class Entity
{
    [Key]
    public long Id { get;  set; }
    
}