using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace StaOpTracking.Data.Models;

public class Company : Entity
{
    public Company()
    {
        this.Employees = new List<Employee>();
    }

    [Required]
    public string Name { get; set; }
    
    public virtual ICollection<Employee> Employees { get; set; }
    
}