using AutoMapper;
using StaOpTracking.Data.Models;

namespace StaOpTracking.Data.MappingProfile;

public static class MappingTest
{
    public static void Test()
    {
        var configuration = new MapperConfiguration(
            cfg => {
                cfg.AddProfile<EmployeeProfile>();
                cfg.AddProfile<CompanyProfile>();
            }
        );
        configuration.AssertConfigurationIsValid();   
    }
}