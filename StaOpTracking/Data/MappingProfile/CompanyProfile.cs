using AutoMapper;
using StaOpTracking.Data.Models;

namespace StaOpTracking.Data.MappingProfile;

public class CompanyProfile : Profile
{
    public CompanyProfile()
    {
        CreateMap<Company, CompanyDTO>()
            .ForMember(dest => dest.Employees,
                opt => opt.MapFrom(src => src.Employees)
            );
        CreateMap<CompanyDTO, Company>()
            .ForMember(dest => dest.Employees,
                opt => opt.MapFrom(src => src.Employees)
            );

        CreateMap<Company, CompanyPostDTO>();
        CreateMap<CompanyPostDTO, Company>()
            .ForMember(dest => dest.Employees, opt => opt.Ignore());
    }
}