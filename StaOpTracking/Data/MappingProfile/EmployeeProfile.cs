using AutoMapper;
using StaOpTracking.Data.Models;

namespace StaOpTracking.Data.MappingProfile;

public class EmployeeProfile : Profile
{
    public EmployeeProfile()
    {
        CreateMap<EmployeeDTO, Employee>()
            .ForMember(dest => dest.Email, opt => opt.Ignore())
            .ForMember(dest => dest.Birthdate, opt => opt.Ignore())
            .ForMember(dest => dest.Gender, opt => opt.Ignore())
            .ForMember(dest => dest.Company, opt => opt.MapFrom(src => src.Company));
        CreateMap<Employee, EmployeeDTO>()
            .ForMember(dest=>dest.Company,opt=>opt.MapFrom(src=>src.Company));
    }
}