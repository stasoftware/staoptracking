using Microsoft.EntityFrameworkCore;
using StaOpTracking.Data.Models;

namespace StaOpTracking.Data.Database;

public static class DataInitialize
{
    public static void Initialize(IServiceProvider serviceProvider)
    {
        using (var context = new DataContext(
                   serviceProvider.GetRequiredService<DbContextOptions<DataContext>>())
              )
        {
            string[] files =
            {
                "./Data/Database/sta_op_tracking.sqlite-shm",
                "./Data/Database/sta_op_tracking.sqlite-wal",
                "./Data/Database/sta_op_tracking.sqlite",
            };
            foreach (string file in files)
            {
                if(File.Exists(@file))
                {
                    File.Delete(@file);
                }
            }
            Console.Out.WriteLine("Removed the database files");

            context.Database.EnsureCreated();
            context.Database.Migrate();

            if (context.Companies.Any())
            {
                return; // DB has been seeded
            }

            //Testing
            HashSet<Company> companies = new HashSet<Company>
            { 
                new Company {Name = "testen"},
                new Company {Name = "testen1"},
                new Company {Name = "testen2"},
                new Company {Name = "testen3"},
            };
            context.Companies.AddRange(
                companies
            );
            context.Employees.Add(
                new Employee()
                {
                    Firstname = "test",
                    Email = "bla@bla.nl",
                    Company = companies.LastOrDefault()
                }
            );

            context.SaveChanges();
        }
    }
}