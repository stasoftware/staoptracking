using Microsoft.EntityFrameworkCore;
using StaOpTracking.Data.Models;

namespace StaOpTracking.Data.Database;

public class DataContext  : DbContext
{
    public DataContext(DbContextOptions<DataContext> options) : base(options)
    {
    }

    public DbSet<Company> Companies { get; set; }
    public DbSet<Employee> Employees { get; set; }

}