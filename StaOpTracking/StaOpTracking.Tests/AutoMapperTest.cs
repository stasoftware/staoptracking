using AutoMapper;
using StaOpTracking.Data.Models;
using Xunit;

namespace StaOpTracking.Tests;

public class AutoMapperTest
{
    [Fact]
    public void Mapping_profiles_should_be_valid()
    {
        var configuration = new MapperConfiguration(
            cfg => cfg.CreateMap<Employee, EmployeeDTO>().ReverseMap()
            );
        configuration.AssertConfigurationIsValid();      
    }
}